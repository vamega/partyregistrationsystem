{-# LANGUAGE OverloadedStrings #-}

import           Control.Monad
import           Snap.Snaplet
import           Snap.Snaplet.SqliteSimple
import qualified Database.SQLite.Simple as S

import qualified Queries as Q

data Person = Person
  {
    rin :: Integer
  , iso :: Integer
  , name :: Integer
  , invitedBy :: Brother
  }

data InviteInfo = InviteInfo
  {
    guestListId :: Integer
  , rin :: Integer
  , usage :: Integer
  , invitedBy :: Integer
  }

data Brother = Brother
  {
    brotherId :: Integer
  , brotherName :: T.Text
  } deriving (Show)

instance FromRow InviteInfo where
  fromRow = InviteInfo <$> field <*> field <*> field <*> field

instance FromRow Brother where
  fromRow = Brother <$>  field <*> field

tableExists :: S.Connection -> String -> IO Bool
tableExists conn tblName = do
  r <- S.query conn
       "SELECT name FROM sqlite_master WHERE type='table' AND name=?"
       (Only ntblName)
  case r of
    [Only (_ :: String)] -> return True
    _ -> return False

-- | Create the necessary database tables, if not already initialized.
createTables :: S.Connection -> IO ()
createTables conn = do
  -- Note: for a bigger app, you probably want to create a 'version'
  -- table too and use it to keep track of schema version and
  -- implement your schema upgrade procedure here.

  partyTableCreated <- tableExists conn "party"
  unless partyTableCreated $
    S.execute_ conn Q.createPartyTable
    
  brotherTableCreated <- tableExists conn "brothers"
  unless brotherTableCreated $
    S.execute_ conn Q.createBrotherTable

  greek_houses <- tableExists conn "greek_houses"
  unless createGreekHousesTable $
    S.execute_ conn Q.createGreekHousesTable

  peopleTableCreated <- tableExists conn "people"
  unless peopleTableCreated $
    S.execute_ conn Q.createPeopleTable

  expressGuestsTableCreated <- tableExists conn "express_guests"
  unless expressGuestsTableCreated $
    S.execute_ conn Q.createExpressGuestsTable

  guestsTableCreated <- tableExists conn "guests"
  unless guestsTableCreated $
    S.execute_ conn Q.createGuestsTable

  doorAdditionsTableCreated <- tableExists conn "doorAdditions"
  unless doorAdditionsTableCreated $
    S.execute_ conn Q.createDoorAdditionsTable

  doorLogTable <- tableExists conn "door_log"
  unless doorLogTable $
    S.execute_ conn Q.createDoorLogTable

lookUpPerson rin partyId = query $ Q.lookUpPerson (rin, partyId)

registerPerson InviteInfo {rin, guestListId} partyId =
  execute $ Q.checkInPerson (rin, partyId, guestListId)
  
peopleInParty = query Q.getAttendanceForParty (Only partyId)
