-- Get party to use.
select max(id)
from party
where datetime(party.date) < datetime('now');

-- Get data for swipe
select
  people.id
  , people.name
  , brothers.name
  , people.greek_house
from
  people
  , greek_houses
  , brothers
  , express_guests
where
  people.rin = ?
  , express_guests.invited_party_id = ?
  , express_guests.inviting_brother_id = brothers.id
  , express_guests.person_rin = people.rin;

-- Get data based on name
select
  guests.id
  , guests.name
  , brothers.name
  , guests.greek_house
from
  greek_houses
  , brothers
  , guests
where
  guests.name = ?
  , guests.invited_party_id = ?
  , guests.greek_house = ?
  , guests.inviting_brother_id = brothers.id
  , guests.person_id is null;

-- Update a guest to associate the entry with the newly created
-- person. This also causes the entry to be marked as utilized.
update guests
set person_id = ?
where guests.id = ?;

-- Get the current number of who've attended this party.
select
  count(DISTINCT *)
from
  doorLog
where
  party_id = ?

-- Create person based on swiped data and name
insert into people
  (rin, iso, greek_house, name)
values
  (?, ?, ?, ?)

-- Create new Party
insert into party
  (date, party_name)
values
  (?, ?)
