import QtQuick 2.0

Image {
    source: "textured_paper.png"
    width: 400
    height: 150
    visible: false

    Rectangle {
        id: background_overlay
        x: 0
        y: 0
        width: 400
        height: 150
        opacity: 0.4
        clip: false
    }

    Text {
        id: name_label
        x: 48
        y: 21
        text: qsTr("Name")
        font.pointSize: 16
    }

    Text {
        id: rin_label
        x: 62
        y: 52
        text: qsTr("RIN")
        font.pointSize: 16
    }

    Text {
        id: invited_by_label
        x: 6
        y: 81
        text: qsTr("Invited By")
        font.pointSize: 16
    }

    Text {
        id: name_text
        x: 141
        y: 21
        visible: false;
        text: qsTr("Text")
        font.pointSize: 16
    }

    Text {
        id: rin_text
        x: 141
        y: 52
        visible: false;
        text: qsTr("Text")
        font.pointSize: 16
    }

    Text {
        id: invited_by_text
        x: 140
        y: 81
        visible: false;
        text: qsTr("Text")
        font.pointSize: 16
    }

    TextInputCustom {
        id: name_input
        x: 140
        y: 21
        width: 220
        height: 23
        visible: false
        text: qsTr("Text")
        input.font.pixelSize: 14
    }

    TextInputCustom {
        id: rin_input
        x: 140
        y: 52
        width: 220
        height: 23
        visible: false
        text: qsTr("Text")
        input.font.pixelSize: 14
    }


    states: [
        State {
            name: "On List With Data"

            PropertyChanges {
                target: background_overlay
                x: 0
                y: 0
                color: "#6cea2d"
            }

            PropertyChanges {
                target: name_input
                visible: false
            }

            PropertyChanges {
                target: rin_input
                visible: false
            }

            PropertyChanges {
                target: name_text
                visible: true
            }

            PropertyChanges {
                target: rin_text
                visible: true
            }

            PropertyChanges {
                target: invited_by_text
                visible: true
            }
        },
        State {
            name: "Unknown"
            PropertyChanges {
                target: background_overlay
                x: 0
                y: 0
                color: "#eae72d"
            }

            PropertyChanges {
                target: name_input
                visible: false
            }

            PropertyChanges {
                target: rin_input
                visible: false
            }

            PropertyChanges {
                target: name_text
                visible: false
            }

            PropertyChanges {
                target: rin_text
                visible: false
            }

            PropertyChanges {
                target: invited_by_text
                visible: true
            }
        },
        State {
            name: "Data Shows not on List"
            PropertyChanges {
                target: background_overlay
                x: 0
                y: 0
                color: "#8d0f0f"
                opacity: 0.75
            }

            PropertyChanges {
                target: name_input
                visible: false
            }

            PropertyChanges {
                target: rin_input
                visible: false
            }

            PropertyChanges {
                target: name_text
                visible: false
            }

            PropertyChanges {
                target: rin_text
                visible: false
            }

            PropertyChanges {
                target: invited_by_text
                visible: false
            }

            PropertyChanges {
                target: invited_by_label
                visible: false
            }
        },
        State {
            name: "Blacklist"
            PropertyChanges {
                target: background_overlay
                x: 0
                y: 0
                color: "#141212"
                opacity: 0.71
            }

            PropertyChanges {
                target: name_input
                visible: false
            }

            PropertyChanges {
                target: rin_input
                visible: false
            }

            PropertyChanges {
                target: name_text
                color: "#dfeaec"
                visible: true
            }

            PropertyChanges {
                target: rin_text
                color: "#dfeaec"
                visible: true
            }

            PropertyChanges {
                target: invited_by_text
                color: "#dfeaec"
                visible: false
            }

            PropertyChanges {
                target: name_label
                color: "#dfeaec"
            }

            PropertyChanges {
                target: invited_by_label
                color: "#dfeaec"
                visible: false
                styleColor: "#000000"
            }

            PropertyChanges {
                target: rin_label
                color: "#dfeaec"
            }
        }
    ]
}
