create table if not exists party (
  id integer primary key autoincrement
  , date datetime not null
  , party_name varchar(255) not null
);

create table if not exists brothers (
  name varchar(255) primary key
  , email varchar(255)
);

create table if not exists greek_houses (
  id integer primary key
  , name varchar(255)
  , letters varchar(255)
);

create table if not exists people (
  rin integer primary key
  , iso integer
  , greek_house varchar(255)
  , name varchar(255)
  , foreign key (greek_house) references greek_houses(name)
);

create table if not exists express_guests (
  id integer primary key
  , inviting_brother_id integer
  , invited_party_id integer
  , person_rin integer
  , foreign key (invited_party_id) references party(id)
  , foreign key (inviting_brother_id) references brothers(id)
  , foreign key (person_rin) references people(rin)
);

create table if not exists guests (
  id integer primary key
  , name varchar(255) not null
  , inviting_brother_id integer not null
  , invited_party_id integer not null
  , person_id integer
  , foreign key (invited_party_id) references party(id)
  , foreign key (inviting_brother_id) references brothers(id)
);

create table if not exists doorAdditions (
  id integer primary key autoincrement
  , person_id integer not null
  , party_id integer not null
  , foreign key(person_id) references people(rin)
  , foreign key(party_id) references party(id)
);

create table if not exists doorLog (
  person_id integer
  , party_id integer
)
