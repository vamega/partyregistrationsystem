 {-# LANGUAGE OverloadedStrings #-}

module Queries where

import           Database.SQLite.Simple
import qualified Data.Text as T

createPartyTable :: Query
createPartyTable = Query $ T.concat [
  "create table if not exists party (" ,
  "id integer primary key autoincrement" ,
  ", date datetime not null" ,
  ", party_name varchar(255) not null" ,
  ")"
  ]

createBrotherTable :: Query
createBrotherTable = Query $ T.concat [
  "create table if not exists brothers (" ,
  "name varchar(255) primary key" ,
  ", email varchar(255)" ,
  ")"
  ]

createGreekHousesTable :: Query
createGreekHousesTable = Query $ T.concat [
  "create table if not exists greek_houses (" ,
  "id integer primary key" ,
  ", name varchar(255)" ,
  ", letters varchar(255)" ,
  ")"
  ]

createPeopleTable :: Query
createPeopleTable = Query $ T.concat [
  "create table if not exists people (" ,
  "rin integer primary key" ,
  ", iso integer" ,
  ", greek_house varchar(255)" ,
  ", name varchar(255)" ,
  ", foreign key (greek_house) references greek_houses(name)" ,
  ")"
  ]

createExpressGuestsTable :: Query
createExpressGuestsTable = Query $ T.concat [
  "create table if not exists express_guests (" ,
  "id integer primary key" ,
  ", inviting_brother_id integer" ,
  ", invited_party_id integer" ,
  ", person_rin integer" ,
  ", foreign key (invited_party_id) references party(id)" ,
  ", foreign key (inviting_brother_id) references brothers(id)" ,
  ", foreign key (person_rin) references people(rin)" ,
  ")" ,
  ]

createGuestsTable :: Query
createGuestsTable = Query $ T.concat [
  "create table if not exists guests (" ,
  "id integer primary key" ,
  ", name varchar(255) not null" ,
  ", inviting_brother_id integer not null" ,
  ", invited_party_id integer not null" ,
  ", person_id integer" ,
  ", foreign key (invited_party_id) references party(id)" ,
  ", foreign key (inviting_brother_id) references brothers(id)" ,
  ")"
  ]

createDoorAdditionsTable :: Query
createDoorAdditionsTable = Query $ T.concat [
  "create table if not exists doorAdditions (" ,
  "id integer primary key autoincrement" ,
  ", person_id integer not null" ,
  ", party_id integer not null" ,
  ", foreign key(person_id) references people(rin)" ,
  ", foreign key(party_id) references party(id)" ,
  ")"
  ]

createDoorLogTable :: Query
createDoorLogTable = Query $ T.concat [
  "create table if not exists door_log (" ,
  "person_id integer" ,
  ", party_id integer" ,
  ")"
  ]


getCurrentParty :: Query
getCurrentParty = Query $ T.concat [
  "select max(id)" ,
  "from party" ,
  "where datetime(party.date) < datetime('now')"
  ]

checkInOnSwipe :: Query
checkInOnSwipe = Query $ T.concat [
  "select" ,
  "people.rin"
  ", people.name" ,
  ", brothers.name" ,
  ", people.greek_house" ,
  "from" ,
  "people" ,
  ", greek_houses" ,
  ", brothers" ,
  ", express_guests" ,
  "where" ,
  "people.rin = ?" ,
  ", express_guests.invited_party_id = ?" ,
  ", express_guests.inviting_brother_id = brothers.id" ,
  ", express_guests.person_rin = people.rin"
  ]

manualCheckIn :: Query
manualCheckIn = Query $ T.concat [
  "select" ,
  "guests.id" ,
  ", guests.name" ,
  ", brothers.name" ,
  ", guests.greek_house" ,
  "from" ,
  "greek_houses" ,
  ", brothers" ,
  ", guests" ,
  "where" ,
  "guests.name = ?" ,
  ", guests.invited_party_id = ?" ,
  ", guests.greek_house = ?" ,
  ", guests.inviting_brother_id = brothers.id" ,
  ", guests.person_id is null"
  ]

createGuest :: Query
createGuest = Query $ T.concat [
  "insert into people" ,
  "(rin, iso, greek_house, name)" ,
  "values" ,
  "(?, ?, ?, ?)"
  ]

associateGuest :: Query
associateGuest = Query $ T.concat [
  "insert into people" ,
  "(rin, iso, greek_house, name)" ,
  "values" ,
  "(?, ?, ?, ?)" ,
  ]

getAttendanceForParty :: Query
getAttendanceForParty = Query $ T.concat [
  "select count(DISTINCT *)" ,
  "from doorLog" ,
  "where party_id = ?"
  ]
