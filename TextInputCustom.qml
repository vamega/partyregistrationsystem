import QtQuick 2.0

Rectangle {
    property alias text: input.text
    property alias maximumLength: input.maximumLength
    property alias input: input

    radius: 3

    color: "#3695c8"
    border.width: 3
    border.color: "#163D52"

    width: 180; height: 28
//    BorderImage {
//        border {left: 10; right: 10;  top: 10; bottom: 10}
//        source: "lineedit.png"
//        anchors.fill: parent
//    }
//    Rectangle
    TextInput {
        id: input
        color: "#151515";
        selectionColor: "green"
        font.pixelSize: 16;
        font.bold: true
        width: parent.width - 16
        anchors.centerIn: parent
        focus: true
    }
}
