{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import           Control.Lens
import           Control.Applicative
import           Snap
--import           Snap.Core
import           Snap.Util.FileServe
--import           Snap.Http.Server
--import           Snap.Snaplet
import           Snap.Snaplet.SqliteSimple

data App = App
  {
    _db :: Snaplet Sqlite
  }

makeLenses ''App

instance HasSqlite (Handler b App) where
   getSqliteState = with db get

main :: IO ()
main = quickHttpServe site

site :: Snap ()
site =
  ifTop (writeBS "hello world") <|>
  route [ ("foo", writeBS "bar")
        , ("echo/:echoparam", echoHandler)
        , ("count", count)
        ] <|>
  dir "static" (serveDirectory ".")

echoHandler :: Snap ()
echoHandler = do
  param <- getParam "echoparam"
  maybe (writeBS "must specify echo/param in URL")
          writeBS param


count :: Snap ()
count = do
  writeBS "5"
