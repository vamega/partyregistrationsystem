var VALID_PATTERN = /;(\d*)=(\d*)0\?/
var currentInput = ""

function readCharacter(key_val, text) {
    currentInput += text
}

function extractDataFromCard(cardReaderInput) {
    var matches = cardReaderInput.match(VALID_PATTERN)

    if(! matches) {
        return false
    }

    var cardData = {
        iso: matches[1],
        rin: matches[2]
    }

    return cardData
}
