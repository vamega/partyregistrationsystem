import QtQuick 2.0
import "CardInput.js" as CardInput

Rectangle {
    id: main_window
    width: 500
    height: 360
    color: "blue"
    focus: true

    Keys.onPressed: {
        if(event.key == Qt.Key_Return)
        {
            var cardData = CardInput.extractDataFromCard(CardInput.currentInput)
            if(cardData == false)
            {
                notice.text = "Try again"
            }
            else
            {
                notice.text = cardData.rin
            }
            CardInput.currentInput = ""
        }
        CardInput.readCharacter(event.key, event.text)
    }


    Image {
        id: image1
        anchors.fill: parent
        fillMode: Image.Tile
        source: "navy_blue.png"
    }

    Widget {
        id: widget1
        x: 50
        anchors.top: notice.bottom
        anchors.topMargin: 25
        anchors.horizontalCenter: notice.horizontalCenter
    }

    Text {
        id: title_text
        text: "Party Registration System"

        color: "#2660D9"
        font.family: "Bauhaus Std"
        font.bold: true
        font.pointSize: 24
        font.letterSpacing: 2

        styleColor: "#c2ccdd"
        style: Text.Outline

        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.horizontalCenter: main_window.horizontalCenter
    }

    Text {
        id: notice
        text: "Swipe Card"

        color: "#9C241F"
        font.bold: true
        font.pointSize: 24
        font.family: "Bauhaus Std"
        font.letterSpacing: 2

        styleColor: "#ddc0c0"
        style: Text.Outline

        anchors.top: title_text.bottom
        anchors.topMargin: 25
        anchors.horizontalCenter: main_window.horizontalCenter

    }
    states: [
        State {
            name: "State1"

            PropertyChanges {
                target: widget1
                visible: true
            }
        }
    ]

}
